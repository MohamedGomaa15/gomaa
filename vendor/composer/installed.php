<?php return array(
    'root' => array(
        'name' => 'gomaa/test',
        'pretty_version' => 'dev-main',
        'version' => 'dev-main',
        'reference' => 'ff272e4718ac43d1c49be8eebc294e496fc8aef3',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'gomaa/test' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'reference' => 'ff272e4718ac43d1c49be8eebc294e496fc8aef3',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
